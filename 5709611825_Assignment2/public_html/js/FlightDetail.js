/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function FlightDetail(price,airline,fromAirport,toAirport,takeoffTime,landingTime){
    var flight = this; //this mun dynamic ,local variable
   // var flights = new Array("Singapore Air" , "Air Asia","Nok Air"); // array
    flight.price = price;
    flight.airline = airline;
    flight.fromAirport =fromAirport;
    flight.toAirport = toAirport;
    flight.takeoffTime = takeoffTime;
    flight.landingTime = landingTime;
    flight.travelHours = FlightDetail.calculateTime(landingTime,takeoffTime); //static method
    flight.transitNumber = 0;
    //keep transit airports between fromAirport and toAirport
    flight.transits = [];
    return{
         get price() {
            return price; //return prototype   //closure  // : value or function
        }, //property{}, property{}
        set price(x) {
            price = x;
        },
        get transits(){
           return transits; 
        },
        set transits(x){
          transits  = x; 
        },
        get airline(){
            return airline;
        },
        get fromAirport(){
            return fromAirport;
        },
        get toAirport(){
            return toAirport;
        },
        get takeoffTime(){
            return takeoffTime;
        },
        get landingTime(){
           return landingTime;  
        },
        get travelTime(){
            return travelTime;
        },
        addTransit: function(transitAirport, transitCountry, transitHours){
             var transit
                    = Object.create({transitAirport: transitAirport, transitCountry: transitCountry, transitHours: transitHours});
            this.transits.push(transit);
        }
    };

 }
    
    FlightDetail.prototype.addTransit = function(transitAirport,transitCountry,transitHours){ //non-static
        var transit = Object.create({transitAirport: transitAirport, transitCountry: transitCountry,
            transitHours: transitHours}); //json:transit
        this.transits.push(transit);
    };
    
    FlightDetail.calculateTime = function(landingTime,takeoffTime){ //static method
        var date1 = new Date(takeoffTime);
        var date2 = new Date(landingTime);
        date1.getHours();
        date2.getHours();
        var hour = date2 - date1;
        return hour;
    };
    var f1 = new FlightDetail(3519,"Singapore Air","BKK","CDG","September 7, 2016  06:00:00",
        "September 8,2016 18:00:00");
        f1.addTransit("SIN", "Singapore", 2.30);
// alert("price  "+f1.price);
        alert(f1.transits[0].transitAirport); 
    

