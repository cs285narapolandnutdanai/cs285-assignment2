//define FlightDetail class 
var p =1;//page
function FlightDetail(price, airline, fromAirport,
        toAirport, takeoffTime, landingTime) {
    var flight =this;
    var p = price;
    var a = airline;
    var from = fromAirport;
    var to = toAirport;
    var takeOff = takeoffTime;
    var landing = landingTime;
    var travelTime = FlightDetail.calculateTime(landingTime, takeoffTime);
    var transits = [];
    return {
        get price() {
            return p; //return prototype   //closure  // : value or function
        }, //property{}, property{}
        set price(x) {
            p = x;
        },
        get transits(){
           return transits; 
        },
        set transits(x){
          transits  = x; 
        },
        get airline(){
            return a;
        },
        get fromAirport(){
            return from;
        },
        get toAirport(){
            return to;
        },
        get takeoffTime(){
            return takeOff;
        },
        get landingTime(){
           return landing;  
        },
        get travelTime(){
            return travelTime;
        },
        addTransit: function (transitAirport, transitCountry, transitHours) { 
            var transit
                    = Object.create({transitAirport: transitAirport, transitCountry: transitCountry, transitHours: transitHours});
            this.transits.push(transit);
        }
    };
};

FlightDetail.calculateTime = function (landingTime, takeoffTime) {
    var date1 = new Date(landingTime);
    var date2 = new Date(takeoffTime);
    if(date1.getHours()> date2.getHours()){
         var hour = date1.getHours() - date2.getHours();
    }
    else{
            hour = date2.getHours() - date1.getHours();
    }
   
    return hour;
};

function paginationR(){
    if(p > num){
        document.getElementById("pR").className = "disabled";
    }
    
   document.getElementById("pC").innerHTML = p;
    p++;
    var l=1;
    
    for(var i=(p*3)-3   ;   i<(p*3)-1 , i<flights.length  ;   i++){
        document.getElementById("price"+l).innerHTML = "฿"+flights[i].price;
        document.getElementById("airline"+l).innerHTML = flights[i].airline;
        document.getElementById("fromAirport"+l).innerHTML = flights[i].fromAirport+": "+ flights[i].takeoffTime;
        document.getElementById("toAirport"+l).innerHTML = flights[i].toAirport+": "+ flights[i].landingTime;
        document.getElementById("travelTime"+l).innerHTML = flights[i].travelTime + " hr";
        l = l+1;
    }  
}
function paginationL(){
   
   if(p === 1){
        document.getElementById("pL").className = "disabled";
    }
    p--;
    document.getElementById("pC").innerHTML = p;
    var l=1;
    var i;
    
    for(i=(p*3)-3   ;   i<(p*3)-1 , i<flights.length  ;   i++){
        document.getElementById("price"+l).innerHTML = "฿"+flights[i].price;
        document.getElementById("airline"+l).innerHTML = flights[i].airline;
        document.getElementById("fromAirport"+l).innerHTML = flights[i].fromAirport+": "+ flights[i].takeoffTime;
        document.getElementById("toAirport"+l).innerHTML = flights[i].toAirport+": "+ flights[i].landingTime;
        document.getElementById("travelTime"+l).innerHTML = flights[i].travelTime + " hr";
        l = l+1;
    }  
    
}
var flights = [];

var f1 = new FlightDetail(3519, "Singapore Air", "BKK"
        , "CDG", "September 7, 2016 15:35:00"
        , "September 8, 2016 05:30:00");
var f2 = new FlightDetail(1500, "Air Asia", "CNX","HDY"
        ,"September 19,2016 5:00:00"
        ,"September 20, 2016 12:00:00");
var f3 = new FlightDetail(3000, "Nok Air", "DMK","HKT"
        ,"September 12,2016 8:00:00"
        ,"September 21, 2016 12:00:00");
var f4 = new FlightDetail(4000, "Thai Air", "DMK","HKT"
        ,"September 12,2016 8:40:00"
        ,"September 21, 2016 12:09:00");
var f5 = new FlightDetail(5000, "Qartar Air", "DMK","HKT"
        ,"September 12,2016 9:30:00"
        ,"September 21, 2016 12:05:00");
var f6 = new FlightDetail(6000, "Emirate Air", "DMK","HKT"
        ," September 12,2016 6:00:00"
        ," September 21, 2016 13:10:00");
 flights.push(f1);
 flights.push(f2);
 flights.push(f3);
 flights.push(f4);
 flights.push(f5);
 flights.push(f6);
 
document.getElementById("numberUpdate").innerHTML = flights.length;
var num = flights.length/3;

 
//alert("Price Before Change:"+f1.price);
f1.addTransit("SIN", "Singapore", 2.30);
//alert(f1.transits[0].transitAirport);
f1.price = 1000; //function f1.price
//alert("Price After Change:"+f1.price);


//f2.addTransit("SIN", "Singapore", 12.00);
//f3.addTransit("SIN", "Singapore", 9.00);

//document.getElementById("price1").innerHTML = "฿"+f1.price; //f1 is model 
//document.getElementById("airline1").innerHTML = f1.airline;
//document.getElementById("fromAirport1").innerHTML = f1.fromAirport;
//document.getElementById("toAirport1").innerHTML = f1.toAirport;
//document.getElementById("travelTime1").innerHTML = f1.travelTime;
//
//document.getElementById("price2").innerHTML = "฿"+f2.price; 
//document.getElementById("airline2").innerHTML = f2.airline;
//document.getElementById("fromAirport2").innerHTML = f2.fromAirport;
//document.getElementById("toAirport2").innerHTML = f2.toAirport;
//document.getElementById("travelTime2").innerHTML = f2.travelTime;
//
//document.getElementById("price3").innerHTML = "฿"+f3.price; 
//document.getElementById("airline3").innerHTML = f3.airline;
//document.getElementById("fromAirport3").innerHTML = f3.fromAirport;
//document.getElementById("toAirport3").innerHTML = f3.toAirport;
//document.getElementById("travelTime3").innerHTML = f3.travelTime;

//document.getElementById("numberUpdate").innerHTML = flights.length;
//document.getElementById("price1").innerHTML = "฿"+flights[0].price;
//document.getElementById("airline1").innerHTML = flights[0].airline;
//document.getElementById("fromAirport1").innerHTML = flights[0].fromAirport +": "+ flights[0].takeoffTime;
//document.getElementById("toAirport1").innerHTML = flights[0].toAirport+": "+ flights[0].landingTime;
//document.getElementById("travelTime1").innerHTML = flights[0].travelTime + " hr";
//
//document.getElementById("price2").innerHTML = "฿"+flights[1].price;
//document.getElementById("airline2").innerHTML = flights[1].airline;
//document.getElementById("fromAirport2").innerHTML = flights[1].fromAirport+": "+ flights[0].takeoffTime;
//document.getElementById("toAirport2").innerHTML = flights[1].toAirport+": "+ flights[0].landingTime;
//document.getElementById("travelTime2").innerHTML = flights[1].travelTime + " hr";
//
//document.getElementById("price3").innerHTML = "฿"+flights[2].price;
//document.getElementById("airline3").innerHTML = flights[2].airline;
//document.getElementById("fromAirport3").innerHTML = flights[2].fromAirport+": "+ flights[0].takeoffTime;
//document.getElementById("toAirport3").innerHTML = flights[2].toAirport+": "+ flights[0].landingTime;
//document.getElementById("travelTime3").innerHTML = flights[2].travelTime + " hr";

var n=1;
for(var i=0;i<flights.length;i++){
    document.getElementById("price"+n).innerHTML = "฿"+flights[i].price;
    document.getElementById("airline"+n).innerHTML = flights[i].airline;
    document.getElementById("fromAirport"+n).innerHTML = flights[i].fromAirport+": "+ flights[i].takeoffTime;
    document.getElementById("toAirport"+n).innerHTML = flights[i].toAirport+": "+ flights[i].landingTime;
    document.getElementById("travelTime"+n).innerHTML = flights[i].travelTime + " hr";
    n = n+1;
}
